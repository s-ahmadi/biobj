# READ ME #
This repository contains the implimentaion of enhanced A\*-based path planners for the weight constrained shortest path problem (WCSPP) and the bi-objective shortest path problem (BOSPP).

* Weight constrained shortest path problem aims to find the shrtest path that satisfies a resource limit.
* Bi-objective shortest problem aims to find a set of Pareto-optimal paths.
## References
If you found any of our implimentations useful, please cite the appropriate references, listed below:

```
@article{Ahmadi2024EnhWCSP,
author = {Ahmadi, Saman and Tack, Guido and Harabor, Daniel and Kilby, Philip and Jalili, Mahdi},
title = {Enhanced methods for the weight constrained shortest path problem},
journal = {Networks},
volume = {84},
number = {1},
pages = {3-30},
keywords = {bi-objective shortest path, constrained pathfinding, heuristic search, weight constrained shortest path},
doi = {https://doi.org/10.1002/net.22210},
url = {https://onlinelibrary.wiley.com/doi/abs/10.1002/net.22210},
year = {2024}
}


```

```
@inproceedings{Ahmadi2022WCSP,
  author    = {Saman Ahmadi and Guido Tack and Daniel Harabor and Philip Kilby},
  title     = {Weight Constrained Path Finding with Bidirectional {A*}},
  booktitle = {Proceedings of the Fifteenth International Symposium on Combinatorial Search, {SOCS} 2022, Vienna, Austria, July 21-23, 2022},
  pages     = {2--10},
  publisher = {{AAAI} Press},
  year      = {2022},
  url       = {https://ojs.aaai.org/index.php/SOCS/article/view/21746} }
```

```
@inproceedings{Ahmadi2021RCSPP,
  author    = {Saman Ahmadi and Guido Tack and Daniel Damir Harabor and Philip Kilby},
  title     = {A Fast Exact Algorithm for the Resource Constrained Shortest Path Problem},
  booktitle = {Thirty-Fifth {AAAI} Conference on Artificial Intelligence, {AAAI} 2021, Virtual Event, February 2-9, 2021},
  pages     = {12217--12224},
  publisher = {{AAAI} Press},
  year      = {2021},
  url       = {https://ojs.aaai.org/index.php/AAAI/article/view/17450} }
```

```
@inproceedings{Ahmadi2021Biobj,
  author    = {Saman Ahmadi and Guido Tack and Daniel Harabor and Philip Kilby},
  title     = {Bi-Objective Search with Bi-Directional {A*}},
  booktitle = {29th Annual European Symposium on Algorithms, {ESA} 2021, September 6-8, 2021, Lisbon, Portugal (Virtual Conference)},
  series    = {LIPIcs},
  volume    = {204},
  pages     = {3:1--3:15},
  publisher = {Schloss Dagstuhl - Leibniz-Zentrum f{\"{u}}r Informatik},
  year      = {2021},
  url       = {https://doi.org/10.4230/LIPIcs.ESA.2021.3},
  doi       = {10.4230/LIPIcs.ESA.2021.3} }
```

### What is this repository for? ###
It contains the implementaion of:

* BOBA\* : A bidirectional A\* search for the BOSPP (parallel search scheme)- Ver.2.0
* BOA\*\_enh : An enhanced unidirectional A\* search for the BOSPP- Ver.2.0
* WC-A\* : A unidirectional A\* search for the WCSPP- Ver.1.1
* WC-BA\* : A bidirectional A\* search for the WCSPP (parallel search scheme, available with the HTL and HTA methods)- Ver.1.1
* WC-EBBA\* : A biased bidirectional A\* search for the WCSPP (sequential search scheme)- Ver.2.0
* WC-EBBA\*\_par : A biased bidirectional A\* search for the WCSPP (Parallel search scheme)- Ver.1.1

Ver2.0. Changelog: Improved runtime and memory efficiency

Algorithms are implemented on top of the Warthog library designed by Daniel Harabor. Please visit [Daniel's repository](https://bitbucket.org/dharabor) if your are inerested in state-of-the-art path planners.
### How do I get set up? ###
Coded in C++ and tested with the GCC 7.5 compiler.
#### To compile
```bash
        make -C ./src
```
`make path` enables solution path construction.
#### To run
```bash
        ./src/bin/roadhog --alg [name]  --input [graph.xy]  --start [id] --goal [id]
```
#### Flags:
```bash
        --help #(outputs manual and available commands)
        --queue [bucket, hybrid] #(priority queue for the main search, default: bucket)
        --constraint [1-100] #(defines a weight limit in % for the WCSPP)
        --path  #(ouputs solution paths)
        --reverse #(swaps source <-> target)
        --noheader  #(stops printing the result header)
        --zero #(disables the heuristic function utilised for DIMACS maps)
        --nruns [x] #(repeats for x times)
```
### Examples:
Running algorithms:
```bash
        ./src/bin/roadhog --alg BOBA        --input ./maps/NY.xy --start 208360 --goal 99990
        ./src/bin/roadhog --alg BOA_enh     --input ./maps/NY.xy --start 15430  --goal 13102  --path
        ./src/bin/roadhog --alg WC_A        --input ./maps/NY.xy --start 1      --goal 264346 --constraint 80
        ./src/bin/roadhog --alg WC_BA       --input ./maps/NY.xy --start 1      --goal 264346 --constraint 10 --queue bucket
        ./src/bin/roadhog --alg WC_EBBA     --input ./maps/NY.xy --start 264346 --goal 1      --constraint 60 --reverse
        ./src/bin/roadhog --alg WC_EBBA_par --input ./maps/NY.xy --start 1      --goal 264346 --constraint 60
```
Note:

* the standard version of the algorithms uses bucket-based queues (bucket for integer and hybrid for non-integer costs) __without__ tie-breaking in the priority queue. __Please do not benchmark with the binary-heap queue or with tie-breaking enabled.__
* the code is designed for benchmark maps of DIMACS with spherical distance as a consistent hueristic. For generic graphs, the heuristic function can be disabled by passing the `--zero` flag to the program.
* the input graph is read 0-indexed, but the vertex identifiers in both input and output of the program are __1-indexed__ (consistent with the DIMACS format).
* the default edge weight type is 32-bit unsigned integer (`uint32`). In case of floating point attributes, the weight type can be set to `double` through the `sys/constant.h` file, and the `--queue hybrid` flag needs to be passed to the program for the correct computation of the solutions.


### Who do I talk to? ###
For reporting issues or requesting changes, please email saman.ahmadi at rmit.edu.au
## Licence
[AGPL-3.0](https://opensource.org/licenses/AGPL-3.0)
